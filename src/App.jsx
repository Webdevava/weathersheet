import React, { useState, useEffect } from "react";
import "./App.scss";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import Tabs from "./components/Tabs";
import Unified from "./tabs/Unified";
import Temp from "./tabs/Temp";
import Wind from "./tabs/Wind";
import Sun from "./tabs/Sun";
import AirPollution from "./tabs/AirPollution";


import logo from './assets/logo.svg'
import logo2 from './assets/logo2.svg'


const App = () => {
  const [favicon, setFavicon] = useState(logo);
  const [searchedLocation, setSearchedLocation] = useState("");
  const [searchSuccess, setSearchSuccess] = useState(false);
  const [recentLocations, setRecentLocations] = useState([]);

  useEffect(() => {
    const faviconImages = [logo, logo2];
    let currentIndex = 0;

    const switchFavicon = () => {
      currentIndex = (currentIndex + 1) % faviconImages.length;
      const newFavicon = faviconImages[currentIndex];

      setFavicon(newFavicon);

      // Update the favicon in the document's head
      const linkElement = document.querySelector("link[rel*='icon']");
      if (linkElement) {
        linkElement.href = newFavicon;
      }
    };

    // Interval to switch favicon every 500 milliseconds
    const intervalId = setInterval(switchFavicon, 500);

    // Cleanup function to clear the interval when the component unmounts
    return () => clearInterval(intervalId);
  }, []); // Effect runs only once when the component mounts

  const handleLocationChange = (location) => {
    setSearchedLocation(location);
  };

  const handleSearch = (success) => {
    setSearchSuccess(success);
  };

  const handleRecentLocations = (locations) => {
    setRecentLocations(locations);
  };

  return (
    <>
       <Navbar
        onLocationChange={handleLocationChange}
        onSearch={handleSearch}
        onRecentLocations={handleRecentLocations}
      />
      <Tabs
        config={[
          { header: "Unified", component: <Unified selectedLocation={searchedLocation}
          recentLocations={recentLocations}/> },
          { header: "Temperatures", component: <Temp /> },
          { header: "Wind", component: <Wind /> },
          { header: "Air Quality", component: <AirPollution /> },
          { header: "Sun", component: <Sun /> },
        ]}
      />
      <Footer/>
    </>
  );
};

export default App;
