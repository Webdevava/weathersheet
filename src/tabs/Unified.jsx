import React from 'react'

const Unified = ({searchedLocation, recentLocations}) => {
  return (
    <div className='unified'>
      <img src="" alt="" />
      <div className="data">
      <p className="weather">{searchedLocation}</p>
      <p className="temp">{recentLocations}</p>
      <p className="wind"></p>
      </div>
    </div>
  )
}

export default Unified
