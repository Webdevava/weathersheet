import React, { useEffect, useState } from "react";
import axios from "axios";
import "../styles/navbar.scss";
import RecentLocationCard from "./RecentLocationCard";
import logo from "../assets/logo.svg";
import locationImg from "../assets/location.svg";
import nolocationImg from "../assets/no_location.svg";
import search from "../assets/search.svg";

const Navbar = ({ onLocationChange, onSearch }) => {
  const [backgroundImageUrl, setBackgroundImageUrl] = useState("");
  const [weatherData, setWeatherData] = useState(null);
  const [location, setLocation] = useState("");
  const [currLocation, setCurrLocation] = useState("Jalna");
  const [searchClicked, setSearchClicked] = useState(false);
  const [recentLocations, setRecentLocations] = useState([]);
  const apiKey = "d3d48be238ecb6ee1713db189f686066";


  useEffect(() => {
    // Fetch a random nature image from Unsplash
    axios
      .get("https://api.unsplash.com/photos/random", {
        params: {
          client_id: "tzzyc18AiUdOx62iVoHSvbp0HPjHQCZRY2UHFCcGcs0",
          query: "nature",
        },
      })
      .then((response) => {
        setBackgroundImageUrl(response.data.urls.regular);
      })
      .catch((error) => {
        console.error("Error fetching image:", error);
      });
  }, []);

  useEffect(() => {
    if (searchClicked && location) {
      axios
        .get(
          `https://api.openweathermap.org/data/2.5/weather?q=${location}&appid=${apiKey}&units=metric`
        )
        .then((response) => {
          setWeatherData(response.data);
          onLocationChange(location); // Pass location to the parent component

          // Filter out duplicates based on location name
          setRecentLocations((prevLocations) => [
            { location,country:response.data.sys.country, temp: response.data.main.temp, feelsLike: response.data.main.feels_like, icon: response.data.weather[0].icon,},
            ...prevLocations.filter((prevLocation) => prevLocation.location !== location)
              .slice(0, 2), // Keep only the latest 3 locations
          ]);

          setSearchClicked(false);
          onSearch(true); // Pass the search state to other components
        })
        .catch((error) => {
          console.error("Error fetching weather data:", error);
          setSearchClicked(false);
          onSearch(false); // Pass the search state to other components
        });
    }
  }, [location, apiKey, searchClicked, onLocationChange, onSearch]);

  const handleSearchClick = () => {
    setSearchClicked(true);
  };

  return (
    <header className="centered" style={{ backgroundImage: `url(${backgroundImageUrl})` }}>
      <nav className="centered">
        <div className="logo centered">
          <img src={logo} alt="" />
          <span>WeatherSheet</span>
        </div>

        <div className="local centered">
          {currLocation ? (
            <>
              <img src={locationImg} alt="" />
              <span>{currLocation}</span>
            </>
          ) : (
            <>
              <img src={nolocationImg} alt="" />
              <span>No location available</span>
            </>
          )}
        </div>
      </nav>
      <div className="inputLocation centered">
        <div className="input centered">
          <input
            type="text"
            placeholder="Enter a location"
            value={location}
            onChange={(e) => setLocation(e.target.value)}
          />
          <button onClick={handleSearchClick}>
            <img src={search} alt="search" />
          </button>
        </div>

        <div className="prevLocations centered">
          {recentLocations.length > 0 && (
            <div>
              <p>Recent locations</p>
              <div className="locations centered">
                {recentLocations.map((recentLocation, index) => (
                  <RecentLocationCard
                    key={index}
                    location={recentLocation.location}
                    country={recentLocation.country}
                    temp={recentLocation.temp}
                    feelsLike={recentLocation.feelsLike}
                    icon={recentLocation.icon}
                  />
                ))}
              </div>
            </div>
          )}

        </div>
      </div>
    </header>
  );
};

export default Navbar;
