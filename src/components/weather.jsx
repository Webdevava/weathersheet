// src/components/Weather.js
import React, { useState, useEffect } from 'react';
import axios from 'axios';

const Weather = () => {
  const [weatherData, setWeatherData] = useState(null);
  const [location, setLocation] = useState('');
  const [searchClicked, setSearchClicked] = useState(false); // State to track whether the search button is clicked
  const apiKey = 'd3d48be238ecb6ee1713db189f686066';

  useEffect(() => {
    if (searchClicked && location) {
      axios
        .get(
          `https://api.openweathermap.org/data/2.5/weather?q=${location}&appid=${apiKey}&units=metric`
        )
        .then((response) => {
          setWeatherData(response.data);
        })
        .catch((error) => {
          console.error('Error fetching weather data:', error);
        })
        .finally(() => {
          setSearchClicked(false); // Reset searchClicked after fetching data
        });
    }
  }, [location, apiKey, searchClicked]);

  const handleSearchClick = () => {
    setSearchClicked(true);
  };

  const getWeatherIconUrl = (iconCode) => {
    return `http://openweathermap.org/img/w/${iconCode}.png`;
  };

  return (
    <div>
      <h2>Weather App</h2>
      <input
        type="text"
        placeholder="Enter a city name"
        value={location}
        onChange={(e) => setLocation(e.target.value)}
      />
      <button onClick={handleSearchClick}>Search</button>
      {weatherData && (
        <div>
        <p>Temperature: {weatherData.main.temp}°C</p>
        <p>Feels like: {weatherData.main.feels_like}°C</p>
        <p>Temperature min: {weatherData.main.temp_min}°C</p>
        <p>Temperature max: {weatherData.main.temp_max}°C</p>
        <p>wind speed: {weatherData.wind.speed}m/s</p>
        <p>Sunrise: {new Date(weatherData.sys.sunrise * 1000).toLocaleTimeString()}</p>
        <p>Sunset: {new Date(weatherData.sys.sunset * 1000).toLocaleTimeString()}</p>
        </div>
      )}
    </div>
  );
};

export default Weather;
