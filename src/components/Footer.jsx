import React from "react";
import '../styles/footer.scss'
import github from '../assets/github.svg'
import linkedin from '../assets/linkedin.svg'
import insta from '../assets/instagram.svg'

const Footer = () => {
  const year = new Date().getFullYear()

  return (
    <footer className="centered">
      <div className="lists centered">
        <ul id="links" className="centered">
          <li>
            <a href="#">API</a>
          </li>
          <li>
            <a href="#">About WeatherSheet</a>
          </li>
          <li>
            <a href="#">Contact Us</a>
          </li>
        </ul>
        <ul id="icons" className="centered">
          <li className="centerd">
            <a href="#">
              <img src={github} alt="" id="github"/>
            </a>
          </li>
          <li className="centerd">
            <a href="#">
              <img src={linkedin} alt="" />
            </a>
          </li>
          <li className="centerd">
            <a href="#">
              <img src={insta} alt="" />
            </a>
          </li>
        </ul>
      </div>
      <p className="copyright centered">
        © {year} WeatherSheet, Inc. All Rights Reserved. 
      </p>
      <p className="love">
      made with ❤️ by <a href="https://github.com/Webdevava">Webdevava</a>
      </p>
    </footer>
  );
};

export default Footer;
